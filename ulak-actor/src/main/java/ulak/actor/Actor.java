package ulak.actor;

/**
 * @param <T>
 */
public interface Actor<T> {

  /**
   * @param input
   */
  void onMessage(T input);

  /**
   * @param signal
   */
  default void onSignal(Signal signal) {
  }
}
