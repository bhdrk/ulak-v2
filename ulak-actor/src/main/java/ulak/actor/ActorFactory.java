package ulak.actor;

/**
 * @param <T>
 */
@FunctionalInterface
public interface ActorFactory<T> {

  /**
   * @return
   */
  Actor<T> create();
}
