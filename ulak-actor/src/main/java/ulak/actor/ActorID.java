package ulak.actor;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class ActorID implements Serializable {

  private static final long serialVersionUID = ActorSystem.VERSION;

  private ActorLink link;
  private long index;

  private ActorID(ActorLink link, long index) {
    this.link = link;
    this.index = index;
  }

  public static ActorID of(ActorLink link, int index) {
    return new ActorID(link, index);
  }

  public ActorLink link() {
    return link;
  }

  public long index() {
    return index;
  }

  @Override
  public String toString() {
    return "ActorID[" + link + "@" + index + "]";
  }
}
