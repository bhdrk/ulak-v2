package ulak.actor;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class ActorLink implements Serializable {
  private final ActorLink parent;
  private final String name;

  private ActorLink(String name) {
    this(null, name);
  }

  private ActorLink(ActorLink parent, String name) {
    this.parent = parent;
    this.name = name;
  }

  public static ActorLink of(String name) {
    return new ActorLink(name);
  }

  public static ActorLink of(ActorLink parent, String name) {
    return new ActorLink(parent, name);
  }

  public ActorLink parent() {
    return parent;
  }

  public String name() {
    return name;
  }

  public boolean hasParent() {
    return parent != null;
  }

  @Override
  public String toString() {
    return (hasParent() ? parent : "") + "/" + name;
  }
}
