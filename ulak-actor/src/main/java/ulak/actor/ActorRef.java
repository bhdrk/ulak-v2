package ulak.actor;

/**
 * @param <T>
 */
public interface ActorRef<T> {

  /**
   * @param input
   */
  void send(T input);
}
