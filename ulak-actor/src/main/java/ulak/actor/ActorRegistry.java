package ulak.actor;

import ulak.actor.options.ActorOptions;

/**
 *
 */
public interface ActorRegistry {

  /**
   * @param options
   */
  void register(ActorOptions options);

  /**
   *
   */
  <T> ActorRef<T> get(ActorLink name);
}
