package ulak.actor;

import ulak.Threads;
import ulak.actor.options.ActorOptions;
import ulak.actor.system.ActorInstanceManager;
import ulak.actor.system.ActorResourceManager;
import ulak.actor.system.ActorSystemConfig;
import ulak.actor.system.ActorSystemMeta;
import ulak.actor.utils.Threads;

import java.util.concurrent.ExecutorService;

public class ActorSystem implements ActorRegistry {

  public static final long VERSION = 1_00;

  private final ActorSystemMeta metadata = new ActorSystemMeta();

  private final ActorSystemConfig config;
  private int coreThread;
  private int maxThread;
  private ExecutorService executor;

  public ActorSystem(ActorSystemConfig config) {
    this.config = config;
  }

  public void start() {
    buildResources();
    buildInstances();
    buildExecutors();
  }

  private void buildExecutors() {

    coreThread = config.getExecutor().getCore() <= 0 ? Runtime.getRuntime().availableProcessors() : config.getExecutor().getCore();
    maxThread = Math.max(coreThread * config.getExecutor().getMaxFactor(), coreThread);
    executor = Threads.newExecutorService(config.getExecutor().getName(), coreThread, maxThread);
  }

  private void buildResources() {
    var arm = new ActorResourceManager(metadata);
    arm.buildResources();
  }

  private void buildInstances() {
    var aim = new ActorInstanceManager(metadata);
    aim.buildInstances();
  }

  @Override
  public void register(ActorOptions options) {
    metadata.addActorOptions(options);
  }

  @Override
  public <T> ActorRef<T> get(ActorLink link) {
    return null;
  }

  public <T> ActorRef<T> ref(String name) {
    return ref(ActorLink.of(name));
  }

  public <T> ActorRef<T> ref(ActorLink link) {
    var resource = metadata.getResource(link);
    return null;
  }
}

