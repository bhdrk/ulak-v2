package ulak.actor;

public class ActorSystemException extends RuntimeException {

  public ActorSystemException() {
    super();
  }

  public ActorSystemException(String message) {
    super(message);
  }

  public ActorSystemException(String message, Throwable cause) {
    super(message, cause);
  }

  public ActorSystemException(Throwable cause) {
    super(cause);
  }
}
