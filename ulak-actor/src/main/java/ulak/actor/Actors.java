package ulak.actor;

import ulak.actor.options.ActorOptions;

public class Actors {
  public static ActorOptions.Builder name(String name) {
    return new ActorOptions.Builder(name);
  }
}
