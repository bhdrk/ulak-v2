package ulak.actor;

/**
 * @param <T>
 */
public interface Mailbox<T> {

  /**
   * @return
   */
  Class<T> messageType();

  /**
   * @param e
   * @return
   */
  boolean add(Message<T> e);

  /**
   * @return
   */
  Message<T> get();

  /**
   * @return
   */
  boolean isEmpty();
}
