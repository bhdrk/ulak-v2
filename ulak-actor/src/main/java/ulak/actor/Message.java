package ulak.actor;

public class Message<T> {

  private ActorID id;
  private T data;

  public Message(ActorID id, T data) {
    this.id = id;
    this.data = data;
  }

  public ActorID id() {
    return id;
  }

  public T data() {
    return data;
  }

  public void free() {
    this.id = null;
    this.data = null;
  }

  public static <T> Message<T> of(ActorID id, T data) {
    return new Message<>(id, data);
  }
}
