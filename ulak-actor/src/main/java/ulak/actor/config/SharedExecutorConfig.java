package ulak.actor.config;

import lombok.Data;

@Data
public class SharedExecutorConfig {
  private Integer coreThread = null;
  private Integer maxThread = null;
}
