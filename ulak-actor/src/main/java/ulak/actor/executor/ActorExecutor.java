package ulak.actor.executor;

import ulak.actor.ActorID;
import ulak.actor.system.ActorResource;

public interface ActorExecutor {

  void submit(ActorResource resource, ActorID id);
}
