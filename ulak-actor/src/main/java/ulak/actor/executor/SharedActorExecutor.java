package ulak.actor.executor;

import ulak.Threads;
import ulak.actor.Actor;
import ulak.actor.ActorID;
import ulak.actor.Mailbox;
import ulak.actor.config.SharedExecutorConfig;
import ulak.actor.system.ActorResource;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.stream.IntStream;

public class SharedActorExecutor implements ActorExecutor {

  private static final String SYSTEM_EXECUTOR_NAME = "ulak-system";

  private final ConcurrentHashMap<ActorID, Task<?>> registry = new ConcurrentHashMap<>();
  private final int coreThread;
  private final int maxThread;
  private final Queue<Task<?>> tasks;
  private Executor executor;

  public SharedActorExecutor(SharedExecutorConfig config, Queue<Task<?>> tasks) {
    this.coreThread = getCoreThread(config);
    this.maxThread = getMaxThread(config);
    this.tasks = tasks;
  }

  private Integer getMaxThread(SharedExecutorConfig config) {
    return config.getMaxThread() != null ? config.getMaxThread() : Runtime.getRuntime().availableProcessors() * 4;
  }

  private Integer getCoreThread(SharedExecutorConfig config) {
    return config.getCoreThread() != null ? config.getCoreThread() : Runtime.getRuntime().availableProcessors();
  }

  public void start() {
    executor = Threads.newExecutorService(SYSTEM_EXECUTOR_NAME, coreThread, maxThread);
    IntStream.range(0, coreThread).forEach(i -> {
      executor.execute(new SharedActorRunner(tasks, i));
    });
  }

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public void submit(ActorResource resource, ActorID id) {
    var task = registry.computeIfAbsent(id, actorID -> {
      var actor = resource.factory().create();
      var mailbox = resource.mailbox();
      return new Task(actorID, actor, mailbox);
    });
    tasks.add(task);
    new AtomicLongArray().set();
    Long.BYTES
  }

}

class SharedActorRunner implements Runnable {

  private final Queue<Task<?>> tasks;
  private final int id;

  public SharedActorRunner(Queue<Task<?>> tasks, int id) {
    this.tasks = tasks;
    this.id = id;
  }

  @Override
  public void run() {
    //noinspection InfiniteLoopStatement
    while (true) {
      //noinspection LoopConditionNotUpdatedInsideLoop
      while (tasks.isEmpty()) {
        Thread.onSpinWait();
      }
      tasks.forEach(this::run0);
    }
  }

  private void run0(Task<?> task) {
    boolean owner = false;
    try {
      if (owner = task.runner.compareAndSet(false, true)) {
        run1(task);
      }
    } finally {
      if (owner) {
        task.runner.compareAndSet(true, false);
      }
    }
  }

  private <T> Mailbox<T> run1(Task<T> task) {
    var actor = task.actor;
    var mailbox = task.mailbox;
    var type = mailbox.messageType();
    var message = mailbox.get();

    if (message != null) {
      var data = message.data();
      var d = type.cast(data);
      try {
        actor.onMessage(d);
      } finally {
        message.free();
      }
    }
    return mailbox;
  }
}

class Task<T> {
  public AtomicBoolean runner = new AtomicBoolean(false);
  public ActorID id;
  public Actor<T> actor;
  public Mailbox<T> mailbox;

  public Task(ActorID id, Actor<T> actor, Mailbox<T> mailbox) {
    this.id = id;
    this.actor = actor;
    this.mailbox = mailbox;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Task<?> task = (Task<?>) o;
    return Objects.equals(id, task.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
