package ulak.actor.mailbox;

import ulak.actor.Mailbox;
import ulak.actor.Message;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @param <T>
 */
public class OrderedQueueMailbox<T> implements Mailbox<T> {

  private ConcurrentLinkedQueue<Message<T>> queue = new ConcurrentLinkedQueue<>();

  private Class<T> type;

  public OrderedQueueMailbox(Class<T> type) {
    this.type = type;
  }

  /**
   * {@inheritDoc}
   *
   * @return
   */
  @Override
  public Class<T> messageType() {
    return type;
  }

  /**
   * {@inheritDoc}
   *
   * @param e
   * @return
   */
  @Override
  public boolean add(Message<T> e) {
    return queue.add(e);
  }

  /**
   * {@inheritDoc}
   *
   * @return
   */
  @Override
  public Message<T> get() {
    return queue.poll();
  }

  /**
   * {@inheritDoc}
   *
   * @return
   */
  @Override
  public boolean isEmpty() {
    return queue.isEmpty();
  }
}
