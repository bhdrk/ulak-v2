package ulak.actor.mailbox;

import ulak.actor.Mailbox;
import ulak.actor.Message;

import java.util.Comparator;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * @param <T>
 */
public class PriorityQueueMailbox<T> implements Mailbox<T> {

  private ConcurrentSkipListSet<Message<T>> queue;

  /**
   * @param comparator
   */
  public PriorityQueueMailbox(Comparator<T> comparator) {
    Comparator<Message<T>> c = (Message<T> m1, Message<T> m2) -> comparator.compare(m1.data(), m2.data());
    queue = new ConcurrentSkipListSet<>(c);
  }

  /**
   * @param e
   * @return
   */
  @Override
  public boolean add(Message<T> e) {
    queue.add(e);
    return false;
  }

  /**
   * @return
   */
  @Override
  public Message<T> get() {
    return queue.pollFirst();
  }

  /**
   * @return
   */
  @Override
  public boolean isEmpty() {
    return queue.isEmpty();
  }
}
