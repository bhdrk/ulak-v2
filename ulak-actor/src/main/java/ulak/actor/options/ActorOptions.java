package ulak.actor.options;

import lombok.EqualsAndHashCode;
import ulak.actor.ActorFactory;

@EqualsAndHashCode
public class ActorOptions {

  private final String name;
  private ActorFactory<?> factory;
  private ExecutorOptions executor;
  private MailboxOptions mailbox;

  private ActorOptions(String name) {
    this.name = name;
  }

  public String name() {
    return name;
  }

  public ActorFactory<?> factory() {
    return factory;
  }

  public ExecutorOptions executor() {
    return executor;
  }

  public MailboxOptions mailbox() {
    return mailbox;
  }

  @Override
  public String toString() {
    return "ActorResource[" + name + "]";
  }

  public static class Builder {

    private final String name;
    private ActorFactory<?> factory;
    private ExecutorOptions executor;
    private MailboxOptions mailbox;

    public Builder(String name) {
      this.name = name;
    }

    public <T> Builder factory(ActorFactory<T> factory) {
      this.factory = factory;
      return this;
    }

    public Builder executor(ExecutorOptions executor) {
      this.executor = executor;
      return this;
    }

    public Builder mailbox(MailboxOptions mailbox) {
      this.mailbox = mailbox;
      return this;
    }

    public ActorOptions build() {
      var options = new ActorOptions(name);
      options.factory = factory;
      options.executor = executor;
      options.mailbox = mailbox;
      return options;
    }
  }
}
