package ulak.actor.options;

import ulak.actor.options.executor.CustomExecutorOptions;
import ulak.actor.options.executor.FixedExecutorOptions;
import ulak.actor.options.executor.SingleExecutorOptions;
import ulak.actor.options.executor.SystemExecutorOptions;

public abstract class ExecutorOptions {

  public enum Type {
    SINGLE, FIXED, SYSTEM, CUSTOM
  }

  public abstract Type type();

  public interface Builder {
    ExecutorOptions build();
  }

  public static SingleExecutorOptions single() {
    return new SingleExecutorOptions();
  }

  public static FixedExecutorOptions fixed() {
    return new FixedExecutorOptions();
  }

  public static SystemExecutorOptions system() {
    return new SystemExecutorOptions();
  }

  public static CustomExecutorOptions custom() {
    return new CustomExecutorOptions();
  }
}
