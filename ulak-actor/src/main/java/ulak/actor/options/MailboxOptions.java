package ulak.actor.options;

import ulak.actor.options.mailbox.OrderedMailboxOptions;
import ulak.actor.options.mailbox.PriorityMailboxOptions;

public abstract class MailboxOptions {

  public enum Type {
    ORDERED, PRIORITY;
  }

  public abstract Type type();

  public interface Builder {
    MailboxOptions build();
  }

  public static OrderedMailboxOptions ordered() {
    return new OrderedMailboxOptions();
  }

  public static PriorityMailboxOptions priority() {
    return new PriorityMailboxOptions();
  }
}
