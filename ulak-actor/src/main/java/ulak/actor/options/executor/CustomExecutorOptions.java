package ulak.actor.options.executor;

import ulak.actor.options.ExecutorOptions;

public class CustomExecutorOptions extends ExecutorOptions {

  public CustomExecutorOptions() {
  }

  @Override
  public Type type() {
    return Type.CUSTOM;
  }

  public static class Builder implements ExecutorOptions.Builder {

    @Override
    public CustomExecutorOptions build() {
      return new CustomExecutorOptions();
    }
  }
}
