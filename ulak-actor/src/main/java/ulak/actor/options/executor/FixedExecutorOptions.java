package ulak.actor.options.executor;

import ulak.actor.options.ExecutorOptions;

public class FixedExecutorOptions extends ExecutorOptions {

  private int count;

  public FixedExecutorOptions() {
  }

  public int count() {
    return count;
  }

  @Override
  public Type type() {
    return Type.FIXED;
  }

  public static class Builder implements ExecutorOptions.Builder {

    private int count;

    public Builder count(int count) {
      this.count = count;
      return this;
    }

    public FixedExecutorOptions build() {
      var options = new FixedExecutorOptions();
      options.count = count;
      return options;
    }
  }
}
