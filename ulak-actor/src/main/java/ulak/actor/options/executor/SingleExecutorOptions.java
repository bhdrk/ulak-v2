package ulak.actor.options.executor;

import ulak.actor.options.ExecutorOptions;

public class SingleExecutorOptions extends ExecutorOptions {

  public SingleExecutorOptions() {
  }

  @Override
  public Type type() {
    return Type.SINGLE;
  }

  public static class Builder implements ExecutorOptions.Builder {

    @Override
    public SingleExecutorOptions build() {
      return new SingleExecutorOptions();
    }
  }
}
