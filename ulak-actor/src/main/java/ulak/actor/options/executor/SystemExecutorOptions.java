package ulak.actor.options.executor;

import ulak.actor.options.ExecutorOptions;

public class SystemExecutorOptions extends ExecutorOptions {

  public SystemExecutorOptions() {
  }

  @Override
  public Type type() {
    return Type.SYSTEM;
  }

  public static class Builder implements ExecutorOptions.Builder {

    @Override
    public SystemExecutorOptions build() {
      return new SystemExecutorOptions();
    }
  }
}
