package ulak.actor.options.mailbox;

import ulak.actor.options.MailboxOptions;

public class OrderedMailboxOptions extends MailboxOptions {

  public OrderedMailboxOptions() {
  }

  @Override
  public Type type() {
    return Type.ORDERED;
  }

  public static class Builder implements MailboxOptions.Builder {

    @Override
    public OrderedMailboxOptions build() {
      return new OrderedMailboxOptions();
    }
  }
}
