package ulak.actor.options.mailbox;

import ulak.actor.options.MailboxOptions;

import java.util.Comparator;

public class PriorityMailboxOptions extends MailboxOptions {

  private Comparator<?> comparator;

  public PriorityMailboxOptions() {
  }

  @Override
  public Type type() {
    return Type.PRIORITY;
  }

  public Comparator<?> comparator() {
    return comparator;
  }

  public static class Builder implements MailboxOptions.Builder {

    private Comparator<?> comparator;

    public Builder() {
    }

    public Builder comparator(Comparator<?> comparator) {
      this.comparator = comparator;
      return this;
    }

    @Override
    public MailboxOptions build() {
      var options = new PriorityMailboxOptions();
      options.comparator = comparator;
      return options;
    }
  }
}
