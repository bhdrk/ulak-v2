package ulak.actor.system;

public class ActorExecutorManager {

  private ActorSystemMeta meta;

  public ActorExecutorManager(ActorSystemMeta meta) {
    this.meta = meta;
  }

  public void buildExecutors() {
    var resources = meta.getResources();
    resources.forEachRemaining(resource -> {
      var type = resource.options().executor().type();
      switch (type){
        case SINGLE:
          break;
        case FIXED:
          break;
        case SYSTEM:
          break;
        case CUSTOM:
          break;
      }
    });
  }
}
