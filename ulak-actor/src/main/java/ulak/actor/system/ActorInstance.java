package ulak.actor.system;

import lombok.EqualsAndHashCode;
import ulak.actor.Actor;
import ulak.actor.ActorID;

@EqualsAndHashCode
public class ActorInstance {

  private final ActorID id;

  private final Actor<?> actor;

  private ActorInstance(ActorID id, Actor<?> actor) {
    this.id = id;
    this.actor = actor;
  }

  public static ActorInstance of(ActorID id, Actor<?> actor) {
    return new ActorInstance(id, actor);
  }

  public ActorID id() {
    return id;
  }

  public Actor<?> actor() {
    return actor;
  }
}
