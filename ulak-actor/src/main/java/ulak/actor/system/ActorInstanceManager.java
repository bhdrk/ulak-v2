package ulak.actor.system;

import ulak.actor.Actor;
import ulak.actor.ActorID;

import java.util.HashMap;

public class ActorInstanceManager {

  private ActorSystemMeta meta;

  public ActorInstanceManager(ActorSystemMeta meta) {
    this.meta = meta;
  }

  public void buildInstances() {
    var resources = meta.getResources();
    var instances = new HashMap<ActorID, Actor<?>>();
    resources.forEachRemaining(resource -> {
      var actor = resource.factory().create();
      var index = resource.index().getAndIncrement();
      var id = ActorID.of(resource.link(), index);
      instances.put(id, actor);
    });
    meta.addInstances(instances);
  }
}
