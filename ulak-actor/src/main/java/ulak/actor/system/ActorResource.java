package ulak.actor.system;

import lombok.EqualsAndHashCode;
import ulak.actor.ActorFactory;
import ulak.actor.ActorLink;
import ulak.actor.Mailbox;
import ulak.actor.executor.ActorExecutor;
import ulak.actor.options.ActorOptions;

import java.util.concurrent.atomic.AtomicInteger;

@EqualsAndHashCode
public class ActorResource {
  private final ActorLink link;
  private final ActorOptions options;
  private final ActorFactory<?> factory;
  private final ActorExecutor executor;
  private final Mailbox<?> mailbox;
  private final AtomicInteger index;

  public ActorResource(ActorLink link, ActorOptions options, ActorFactory<?> factory, ActorExecutor executor, Mailbox<?> mailbox) {
    this.link = link;
    this.options = options;
    this.factory = factory;
    this.executor = executor;
    this.mailbox = mailbox;
    this.index = new AtomicInteger(0);
  }

  public ActorLink link() {
    return link;
  }

  public ActorOptions options() {
    return options;
  }

  public ActorFactory<?> factory() {
    return factory;
  }

  public Mailbox<?> mailbox() {
    return mailbox;
  }

  public ActorExecutor executor() {
    return executor;
  }

  public AtomicInteger index() {
    return index;
  }
}

