package ulak.actor.system;

import ulak.actor.ActorLink;
import ulak.actor.ActorSystemException;
import ulak.actor.Mailbox;
import ulak.actor.executor.ActorExecutor;
import ulak.actor.executor.SingleActorExecutor;
import ulak.actor.mailbox.OrderedQueueMailbox;
import ulak.actor.mailbox.PriorityQueueMailbox;
import ulak.actor.options.ActorOptions;
import ulak.actor.options.ExecutorOptions;
import ulak.actor.options.MailboxOptions;
import ulak.actor.options.executor.SingleExecutorOptions;
import ulak.actor.options.mailbox.OrderedMailboxOptions;
import ulak.actor.options.mailbox.PriorityMailboxOptions;

import java.util.HashMap;

public class ActorResourceManager {


  private ActorSystemMeta meta;

  public ActorResourceManager(ActorSystemMeta meta) {
    this.meta = meta;
  }

  public void buildResources() {
    var records = meta.getActorOptions();
    var builds = new HashMap<ActorLink, ActorResource>();
    records.forEachRemaining(options -> {
      var resource = build(options);
      builds.put(resource.link(), resource);
    });
    meta.addResources(builds);
  }

  public ActorResource build(ActorOptions actorOptions) {
    var link = ActorLink.of(actorOptions.name());
    var factory = actorOptions.factory();
    var mailbox = createMailbox(actorOptions);
    var executor = createExecutor(actorOptions);
    return new ActorResource(link,actorOptions, factory, executor, mailbox);
  }

  private ActorExecutor createExecutor(ActorOptions actorOptions) {
    var executorOptions = actorOptions.executor();
    if (executorOptions == null) {
      executorOptions = ExecutorOptions.system();
    }

    if (executorOptions instanceof SingleExecutorOptions) {
      return new SingleActorExecutor();
    } else {
      throw new ActorSystemException("Invalid executor options for actor " + actorOptions);
    }
  }

  private Mailbox<?> createMailbox(ActorOptions actorOptions) {
    var mailboxOptions = actorOptions.mailbox();
    if (mailboxOptions == null) {
      mailboxOptions = MailboxOptions.ordered();
    }
    if (mailboxOptions instanceof OrderedMailboxOptions) {
      return new OrderedQueueMailbox<>(type);
    } else if (mailboxOptions instanceof PriorityMailboxOptions) {
      var options = (PriorityMailboxOptions) mailboxOptions;
      return new PriorityQueueMailbox<>(options.comparator());
    } else {
      throw new ActorSystemException("Invalid mailbox options for actor " + actorOptions);
    }
  }
}
