package ulak.actor.system;

import lombok.Data;

@Data
public class ActorSystemConfig {

  private ExecutorConfig executor = new ExecutorConfig();

  @Data
  public static class ExecutorConfig {
    private String name = "system";
    private int core = 0;
    private int maxFactor = 4;
  }
}
