package ulak.actor.system;

import ulak.actor.Actor;
import ulak.actor.ActorID;
import ulak.actor.ActorLink;
import ulak.actor.options.ActorOptions;

import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class ActorSystemMeta {

  private final Queue<ActorOptions> registry = new ConcurrentLinkedQueue<>();
  private final ConcurrentMap<ActorLink, ActorResource> resources = new ConcurrentHashMap<>();
  private final ConcurrentMap<ActorID, Actor<?>> instances = new ConcurrentHashMap<>();

  public void addActorOptions(ActorOptions options) {
    registry.add(options);
  }

  public Iterator<ActorOptions> getActorOptions() {
    return registry.iterator();
  }

  public Iterator<ActorResource> getResources() {
    return resources.values().iterator();
  }

  public ActorResource getResource(ActorLink link) {
    return resources.get(link);
  }

  public void addResources(Map<ActorLink, ActorResource> values) {
    resources.putAll(values);
  }

  public void addInstances(Map<ActorID, Actor<?>> values) {
    instances.putAll(values);
  }

  public Actor<?> getInstance(ActorID id) {
    return instances.get(id);
  }
}
