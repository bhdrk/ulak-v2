package ulak.actor.system;

import ulak.actor.ActorID;
import ulak.actor.ActorRef;
import ulak.actor.Mailbox;
import ulak.actor.Message;

public class DefaultActorRef<T> implements ActorRef<T> {

  private final MessageDispatcher dispatcher;
  private Mailbox<T> mailbox;
  private final ActorID id;

  public DefaultActorRef(MessageDispatcher dispatcher, Mailbox<T> mailbox, ActorID id) {
    this.dispatcher = dispatcher;
    this.mailbox = mailbox;
    this.id = id;
  }

  @Override
  public void send(T data) {
    var message = Message.of(id, data);
    if (mailbox.add(message)){
      dispatcher.send(message);
    }
  }
}
