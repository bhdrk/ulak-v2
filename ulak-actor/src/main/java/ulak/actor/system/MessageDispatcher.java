package ulak.actor.system;

import ulak.actor.Message;

/**
 *
 */
public class MessageDispatcher {

  private final ActorSystemMeta metadata;

  public MessageDispatcher(ActorSystemMeta metadata) {
    this.metadata = metadata;
  }

  /**
   * @param message
   */
  public void send(Message<?> message) {
    var actorResource = metadata.getResource(message.id().link());

    //queue.add(new MessageBox(link, data));
  }

  /**
   * @param message
   */
  public void unhandled(Message<?> message) {
    // TODO: Process unhandled messages
  }
}
