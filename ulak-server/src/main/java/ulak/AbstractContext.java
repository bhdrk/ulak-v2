package ulak;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

public class AbstractContext {

  private final ConcurrentMap<String, Object> instances = new ConcurrentHashMap<>();

  @SuppressWarnings("unchecked")
  protected <T> T single(String name, Supplier<T> factory) {
    return (T) instances.computeIfAbsent(name, s -> factory.get());
  }

  protected <T> CompletableFuture<T> completed(T value) {
    return CompletableFuture.completedFuture(value);
  }

  protected <T> CompletableFuture<T> async(Supplier<T> supplier) {
    return CompletableFuture.supplyAsync(supplier);
  }
}
