package ulak;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import ulak.config.DiscoveryConfig;
import ulak.config.NodeConfig;
import ulak.raft.rpc.RaftRPCGrpc;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

public class DiscoveryChannel {

  private final DiscoveryConfig config;

  private final ConcurrentMap<String, String> status = new ConcurrentHashMap<>();

  private Executor executor;

  public DiscoveryChannel(DiscoveryConfig config, ExecutorManager em) {
    this.config = config;
    this.executor = em.get(PredefinedExecutors.DISCOVERY);
  }

  public CompletableFuture<Void> start() {
    return CompletableFuture.runAsync(() -> {
      var holders = config.getNodes().stream()
        .map(this::createChannel)
        .collect(Collectors.toSet());

      holders.forEach(h -> {
        h.getChannel().notifyWhenStateChanged(ConnectivityState.READY, () -> {
          this.updateState(h.getId());
        });
        h.getChannel().getState(true);
      });
    }, executor);
  }

  private CompletableFuture<Void> updateState(String nodeId) {
    return CompletableFuture.runAsync(() -> {
    }, executor);
  }

  private ChannelHolder createChannel(NodeConfig node) {
    var channel = ManagedChannelBuilder.forTarget(node.getAddress())
      .usePlaintext()
      .build();
    var stub = RaftRPCGrpc.newStub(channel);
    return new ChannelHolder(node.getId(), channel, stub);
  }
}

@Data
@AllArgsConstructor
class ChannelHolder {
  private String id;
  private ManagedChannel channel;
  private RaftRPCGrpc.RaftRPCStub stub;
}
