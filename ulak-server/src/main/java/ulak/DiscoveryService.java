package ulak;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class DiscoveryService {

  private ScheduledExecutorService executor;

  public DiscoveryService(ScheduledExecutorService executor) {
    this.executor = executor;
  }

  public void start() {
    final int delay = ThreadLocalRandom.current().nextInt(0, 100);
    executor.schedule(() -> {
    }, delay, TimeUnit.MILLISECONDS);
  }
}
