package ulak;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;

public class ExecutorManager {

  private ConcurrentMap<PredefinedExecutors, Executor> executors = new ConcurrentHashMap<>();

  public void start() {
    for (PredefinedExecutors value : PredefinedExecutors.values()) {
      executors.computeIfAbsent(value, s -> value.createExecutor());
    }
  }

  public Executor get(PredefinedExecutors pe) {
    return executors.get(pe);
  }
}
