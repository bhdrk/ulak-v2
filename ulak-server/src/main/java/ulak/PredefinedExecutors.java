package ulak;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public enum PredefinedExecutors {

  DISCOVERY() {
    @Override
    public Executor createExecutor() {
      return Executors.newSingleThreadExecutor();
    }
  };

  public abstract Executor createExecutor();
}
