package ulak;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class Ulak {

  private final UlakContext context;

  public Ulak() {
    this.context = new UlakContext();
  }

  public void start() throws IOException {
    log.info("Server started...");
    var rpcServer = this.context.rpcServer().join();
    rpcServer.start();
  }

  public void waitFor() throws InterruptedException {
    var rpcServer = this.context.rpcServer().join();
    rpcServer.waitFor();
  }
}
