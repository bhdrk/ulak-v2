package ulak;

import ulak.config.ConfigFactory;
import ulak.config.UlakConfig;
import ulak.rpc.RpcServer;
import ulak.rpc.RpcServerBuilder;
import ulak.rpc.service.RaftRpcService;

import java.util.concurrent.CompletableFuture;

public class UlakContext extends AbstractContext {

  public CompletableFuture<UlakConfig> ulakConfig() {
    return single("ulakConfig", () -> async(() -> {
      var factory = new ConfigFactory();
      return factory.create();
    }));
  }

  public CompletableFuture<RpcServerBuilder> rpcServerBuilder() {
    return single("rpcServerBuilder",
      () -> ulakConfig()
        .thenApply(ulakConfig -> new RpcServerBuilder(ulakConfig.getServer()))
        .thenCombine(raftRpcService(), RpcServerBuilder::addService)
    );
  }

  public CompletableFuture<RpcServer> rpcServer() {
    return single("rpcServer",
      () -> rpcServerBuilder().thenApply(RpcServer::new)
    );
  }

  public CompletableFuture<RaftRpcService> raftRpcService() {
    return single("raftRpcService",
      () -> completed(new RaftRpcService())
    );
  }
}
