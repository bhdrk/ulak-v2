package ulak.config;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class DiscoveryConfig {
  private Set<NodeConfig> nodes;
}
