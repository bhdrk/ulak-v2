package ulak.config;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class NodeConfig {
  private final String id;
  private final String address;
}
