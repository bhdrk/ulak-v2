package ulak.config;

import lombok.Data;

@Data
public class UlakConfig {

  private RpcServerConfig server = new RpcServerConfig();

  private DiscoveryConfig discovery = new DiscoveryConfig();

  private ExecutorConfig executor = new ExecutorConfig();

  @Data
  public static class RpcServerConfig {
    private String host = "127.0.0.1";
    private int port = 5001;
  }

  @Data
  public static class ExecutorConfig {
    private String name = "system";
    private int core = 0;
    private int maxFactor = 4;
  }
}
