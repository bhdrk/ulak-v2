package ulak.rpc;

import io.grpc.Server;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class RpcServer {

  private final RpcServerBuilder serverBuilder;

  public RpcServer(RpcServerBuilder serverBuilder) {
    this.serverBuilder = serverBuilder;
  }

  private Server server;

  public void start() throws IOException {
    this.server = serverBuilder.build();
    this.server.start();

    this.registerShutdownHook();
  }

  public void stop() throws InterruptedException {
    if (server != null) {
      server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
    }
  }

  public void waitFor() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  private void registerShutdownHook() {
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      try {
        this.stop();
      } catch (InterruptedException e) {
        e.printStackTrace(System.err);
      }
    }));
  }
}
