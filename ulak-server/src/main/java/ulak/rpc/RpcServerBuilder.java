package ulak.rpc;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerServiceDefinition;
import ulak.config.UlakConfig;

public class RpcServerBuilder {

  private final ServerBuilder<?> builder;

  public RpcServerBuilder(UlakConfig.RpcServerConfig config) {
    this.builder = ServerBuilder.forPort(config.getPort());
  }

  public RpcServerBuilder addService(ServerServiceDefinition service) {
    this.builder.addService(service);
    return this;
  }

  public RpcServerBuilder addService(BindableService service) {
    this.builder.addService(service);
    return this;
  }

  public Server build() {
    return this.builder.build();
  }
}
