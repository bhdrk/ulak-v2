package ulak.rpc.service;

import io.grpc.stub.StreamObserver;
import ulak.raft.rpc.*;

public class RaftRpcService extends RaftRPCGrpc.RaftRPCImplBase {

  @Override
  public void appendEntries(AppendEntriesRequest request, StreamObserver<AppendEntriesResponse> result) {
    result.onNext(AppendEntriesResponse.newBuilder().build());
    result.onCompleted();
  }

  @Override
  public void requestVote(VoteRequest request, StreamObserver<VoteResponse> result) {
    result.onNext(VoteResponse.newBuilder().build());
    result.onCompleted();
  }
}
